<?php

use yii\helpers\Html;
use app\assets\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target=".nav-container" id="top">

<?php $this->beginBody() ?>
<div class="container wrapper">

    <div>
        <?= $content ?>
    </div>

    <footer>

        <div class="copyright container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="copyright-text">
                        © 2016-<?= date('Y') ?> СТУДИЯ МАНИКЮРА "MONAILSTUDIO"
                    </div>
                </div>
            </div>
        </div>

    </footer>

    <nav id="sub-nav" class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <span style="color:#fcdbdf;font-weight: bold">MO</span>NAIL STUDIO
                </a>
            </div>
            <p class="navbar-text">
                <strong><a href="tel:+79257777541">+7 925 7777-541</a></strong>
                /  WhatsApp
                /  Viber
            </p>
        </div>
    </nav>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
