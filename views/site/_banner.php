<?php
$altText = '';
$images = \yii\helpers\FileHelper::findFiles(Yii::getAlias('@webroot/images/banners/'));
$images = array_map(function($a){return basename($a);}, $images);
sort($images);
?>
<div id="owl-top" class="owl-carousel owl-theme">
    <?php foreach ($images as $image) : ?>
        <div class="item">
            <img src="<?= Yii::getAlias('@web/images/banners/'. $image) ?>"
                 alt="<?= $altText ?>">
        </div>
    <?php endforeach; ?>
</div>