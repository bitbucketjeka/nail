<?php
$this->title = 'Санвади. Акции, скидки. Витебск. Двери, окна, балконы, тамбуры,  шкафы купе, корпусная мебель в Витебске. Sanvadi.';
?>
<div class="nav-container">
    <?php
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;

    NavBar::begin(
        [
            'id' => 'main-nav',
            'brandLabel' => 'SANVADI',
            'brandUrl' => '/',
            'options' => [
                'class' => 'navbar-fixed-top navbar-default',
            ],
        ]
    );
    echo Nav::widget(
        [
            'encodeLabels' => false,
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => '<span class="glyphicon glyphicon-home"></span>', 'url' => ['/site/index']],
                ['label' => 'Как нас найти?', 'url' => Yii::$app->urlManager->createUrl(['/site/index']) . '#contact'],
                ['label' => 'Акции', 'url' => ['/site/discount']],
            ],
        ]
    );

    NavBar::end();
    ?>
</div>

<div class="container">
    <h1 class="page-header text-danger">Действующие акции</h1>

    <h2>Весенние сны сбываются (<strong>с 1 по 31 марта 2016</strong>)</h2>

    <div class="row">
        <div class="col-md-6">
            На период проведения акции всем розничным покупателям будет предоставляться скидка 20% при покупке
            матрасов из коллекций «Комфорт» и «Модерн».
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="http://www.vegas.by/media/catalog/product/cache/1/small_image/152x/9df78eab33525d08d6e5fb8d27136e95/s/m/sm-mattress-03.gif"">
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="http://www.vegas.by/media/catalog/product/cache/1/small_image/152x/9df78eab33525d08d6e5fb8d27136e95/b/s/bs-mattress-m1.jpg"">
                </div>
            </div>
        </div>
    </div>

    <h2>Мебель по низким ценам:</h2>

    <div class="row">

        <div class="col-xs-6">
            <p class="lead">
                Кухня всего лишь за <strong>2 350 000 руб.</strong>
            </p>
            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="/images/banquettes/1.jpg"">
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="/images/banquettes/2.jpg"">
                </div>
            </div>
        </div>

        <div class="col-xs-6">

            <p class="lead">
                Банкетка - <strong>400 000 руб</strong>.
            </p>

            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="/images/banquettes/banquettes3.jpg">
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="/images/banquettes/banquettes4.jpg">
                </div>
            </div>

        </div>

    </div>

    <h2>Покупайте окна вместе с друзьями</h2>

    <div class="row">

        <div class="col-xs-6">

            <p class="lead">
                Посоветуйте нашу фирму вашим друзьям, знакомым, соседям и мы подарим вам <a
                    href="<?= Yii::$app->urlManager->createUrl(['/site/index']) . '#cell' ?>">москитную сетку</a> . Она
                надежно защитит Ваш дом от насекомых и уличного мусора. Или раздвижную антимосктиную сетку на балкон с
                50% скидкой. Сумма заказа выших друзей должна быть больше 5 млн бел. рублей.
            </p>

        </div>

    </div>

    <h2>Заказывайте у нас мебель и получайте подарки</h2>

    <div class="row">

        <div class="col-xs-6">

            <p class="lead">
                <strong>Рекомендуйте нашу фирму вашим друзьям, знакомым, соседям.</strong> И при их заказе на сумму
                5.000.000 рублей, вы получите от нас дисконтрую карту и банкетку в подарок.
            </p>

        </div>

    </div>

    <h2>Постоянные скидки.</h2>

    <p class="lead">
        Постоянным и оптовым клиентам мы всегда предоставляем скидки.
    </p>

    <p class="lead">
        Для пенсионеров у нас возможно оформить <strong>беспроцентную рассрочку на три месяца</strong>.
    </p>

    <h1 class="text-center">
        <a href="#" onclick="jivo_api.open();">
            Наш консультант всегда готов ответить на ваши вопросы.
        </a>
    </h1>

    <hr>

    <h1 class="page-header text-danger">Завершённые акции</h1>

    <h2>Новогодние подарки</h2>

    <div class="row">
        <div class="col-md-6">
            С <strong>20 декабря 2015 по 07 января 2016</strong> каждому заказчику - новогодний подарок от фирмы!
        </div>
        <div class="col-md-6">
            <img class="img-responsive" src="http://gorodvitebsk.by/BANNER/sanvadi.gif">
        </div>
    </div>

    <h2>Подарки при заказе балкона</h2>

    <div class="row">
        <div class="col-md-6">
            Успей <strong>до 31 декабря 2015г.</strong> при <a href="/#works1">заказе балкона</a> получить подарок на
            выбор (москитная сетка
            либо вешалка для белья с установкой)!
        </div>
        <div class="col-md-3">
            <img src="/images/cell.jpg" class="img-responsive">
        </div>
        <div class="col-md-3">
            <img src="/images/works/owl34.jpg" class="img-responsive">
        </div>

    </div>

    <h3 class="text-success">Банкетка в подарок
        <small><a href="http://gorodvitebsk.by/priz/sanvadi" target="_blank">Придумайте для нашей компании оригинальный
                слоган
                и получите банкетку.</a></small>
    </h3>

    <div class="row">
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
                <img src="/images/banquettes/banquettes3.jpg" alt="Санвади. Витебск. Банкетка. Пуф.">
            </a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
                <img src="/images/banquettes/banquettes4.jpg" alt="Санвади. Витебск. Банкетка. Пуф.">
            </a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
                <img src="/images/banquettes/banquettes1.jpg" alt="Санвади. Витебск. Банкетка. Пуф.">
            </a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
                <img src="/images/banquettes/banquettes2.jpg" alt="Санвади. Витебск. Банкетка. Пуф.">
            </a>
        </div>
    </div>

    Банкетка создаст атмосферу уюта и комфорта.
    <ul>
        <li>Устойчивая, компактная и удобная для сидения</li>
        <li>Уход за тканями и экокожей невероятно легок и прост - банкетка прослужит долго и сохранит свой
            презентабельный вид
        </li>
        <li>Банкетка создаст дополнительный комфорт и уют в любой комнате вашего дома</li>
    </ul>

</div>