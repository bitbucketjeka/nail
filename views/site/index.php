<?php
/**
 * @var yii\web\View $this
 */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$this->title = 'МОСКВА СТУДИЯ МАНИКЮРА МАНИКЮР•ПЕДИКЮР•БРОВИ КУРСЫ МАНИКЮРА Ⓜ Новокузнецкая 2 мин. Ⓜ Третьяковская 5 мин. 📱+7 925 7777 541 WhatsApp/Viber';
?>
<?php
NavBar::begin(
    [
        'id' => 'main-nav',
        'brandLabel' => false,
        'options' => [
            'class' => 'navbar-fixed-top navbar-default',
        ],
    ]
);
echo Nav::widget(
    [
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => '<span class="glyphicon glyphicon-home"></span>', 'url' => '#top'],
            ['label' => 'УСЛУГИ', 'url' => '#prices'],
            ['label' => 'КАК НАС НАЙТИ?', 'url' => '#contact'],
            ['label' => 'НАШ ИНСТАГРАМ', 'url' => 'https://www.instagram.com/monailstudio/'],

        ],
    ]
);
NavBar::end();
?>

<?= $this->render('_banner') ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="prices">
                <?= $this->render('_price') ?>
            </div>
        </div>
    </div>
</div>

<div id="contact" class="footer-wrapper container" itemscope itemtype="http://schema.org/Organization">
    <div class="row">
        <div class="col-xs-12">
            <h3>Мы находимся в Москве</h3>
            ул. Пятницкая 8 (проход через арку, налево в подъезд, второй этаж)<br>
            Ⓜ️Новокузнецкая 2 мин / Ⓜ️Третьяковская 5 мин
            <div class="text-center">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe allowfullscreen class="embed-responsive-item"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d561.4595306716379!2d37.627082729272!3d55.743941553682006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54afeb998765f%3A0x1edb766bc316cd9e!2sMonail+Studio!5e0!3m2!1sru!2sby!4v1521639524290"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
